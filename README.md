### Usage

```golang
package markov

import (
	"fmt"
	"io/ioutil"
)

func main() {
	b, err := ioutil.ReadFile("book.txt")
	must(err)

	m := New(4)

	err = m.LoadText(string(b))
	must(err)

	s, err := m.GenerateSentences(5, true)
	must(err)

	fmt.Println(s)

	err = m.ToFile("test.markov")
	must(err)
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
```