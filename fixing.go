package markov

import (
	"regexp"
	"strings"
)

var (
	sentenceFixes = map[string]string{
		"it 's":    "it's",
		"he 's":    "he's",
		"she 's":   "she's",
		"that 's":  "that's",
		"there 's": "there's",
		"they 're": "they're",
		"you 're":  "you're",
		"you 'll":  "you'll",
		"i 'll":    "i'll",
		"we 're":   "we're",
		"i 'm":     "i'm",
	}
	fixes = map[string]string{
		"not es": "notes",
		"ca not": "cannot",
		"wo not": "wont",
		"n't":    "not",
	}
)

func FixSentence(sentence []string) string {
	str := strings.Join(sentence, " ")

	rgx := regexp.MustCompile(`"{2,}`)
	str = rgx.ReplaceAllString(str, "\"")

	rgx = regexp.MustCompile(`'{2,}`)
	str = rgx.ReplaceAllString(str, "'")

	rgx = regexp.MustCompile(`(?m)\s+('s|'re|'m|'ll)`)
	for _, match := range rgx.FindAllString(str, -1) {
		str = strings.ReplaceAll(str, match, strings.TrimSpace(match))
	}

	for key, val := range fixes {
		str = strings.ReplaceAll(str, key, val)
	}

	for key, val := range sentenceFixes {
		str = strings.ReplaceAll(str, key, val)
		str = strings.ReplaceAll(str, ucfirst(key), ucfirst(val))

		rgx = regexp.MustCompile(`(?mi)(?: |^)` + val)
		for _, match := range rgx.FindAllString(str, -1) {
			str = strings.ReplaceAll(str, match, match+" ")
		}
	}

	rgx = regexp.MustCompile(`(?m)"+( *.*? *)"+`)
	for _, match := range rgx.FindAllStringSubmatch(str, -1) {
		str = strings.ReplaceAll(str, match[0], " \""+strings.TrimSpace(match[1])+"\" ")
	}

	rgx = regexp.MustCompile(`(?m)([a-z])([A-Z])`)
	for _, match := range rgx.FindAllStringSubmatch(str, -1) {
		str = strings.ReplaceAll(str, match[0], match[1]+" "+match[2])
	}

	rgx = regexp.MustCompile(`(?m) {2,}`)
	for _, match := range rgx.FindAllString(str, -1) {
		str = strings.ReplaceAll(str, match, " ")
	}

	rgx = regexp.MustCompile(`(?m)\s+[,.!?;:]`)
	for _, match := range rgx.FindAllString(str, -1) {
		str = strings.ReplaceAll(str, match, strings.TrimSpace(match))
	}

	for key, val := range fixes {
		str = strings.ReplaceAll(str, key, val)
	}

	return strings.TrimSpace(ucfirst(str))
}
