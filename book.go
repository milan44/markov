package markov

import (
	"errors"
	"regexp"
	"strings"
)

type BookConfig struct {
	CharactersPerPage int
	Pages             int
	MaxTitleLength    int
}

type Book struct {
	Title string
	Pages []string
}

func (m Markov) GenerateBook(b BookConfig) (Book, error) {
	book := Book{
		Title: "",
		Pages: make([]string, 0),
	}

	m._mutex.Lock()
	table := m.Table
	m._mutex.Unlock()

	if len(table) < 1 {
		return book, errors.New("empty table")
	}

	char := m.getStart(table)
	temp := make([]string, 0)
	out := make([]string, 0)

	iterations := int64(0)
	dotEncounter := 0

	page := ""

	for {
		if iterations > MaximumIterations {
			return book, errors.New("maximum iterations hit")
		}

		newChar := m.Next(char, table)

		newChars := strings.Split(newChar, " ")
		ch := newChars[len(newChars)-1]

		temp = append(temp, ch)

		if dotEncounter > 0 {
			out = append(out, ch)

			if hasEnded(out) {
				if book.Title == "" {
					t := FixSentence(out)
					rgx := regexp.MustCompile(`(?m)[.,?!]+$`)
					t = rgx.ReplaceAllString(t, "")

					if len([]rune(t)) > b.MaxTitleLength {
						out = make([]string, 0)
						dotEncounter = 0
					} else {
						book.Title = t
						out = make([]string, 0)
						iterations = 0
					}
				} else {
					iterations = 0
					page += " " + strings.Join(out, " ")
					page = strings.TrimSpace(page)
					out = make([]string, 0)

					if len([]rune(page)) >= b.CharactersPerPage {
						book.Pages = append(book.Pages, FixSentence([]string{page}))
						page = ""

						if len(book.Pages) > b.Pages {
							break
						}
					}
				}
			}
		}

		if hasEnded(temp) {
			dotEncounter++
		}

		char = newChar

		iterations++
	}

	return book, nil
}
