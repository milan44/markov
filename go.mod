module gitlab.com/milan44/markov

go 1.15

require (
	github.com/imjasonmiller/godice v0.1.2
	github.com/jdkato/prose v1.2.1
	github.com/jdkato/prose/v2 v2.0.0
	github.com/mozillazg/go-unidecode v0.1.1
	github.com/pkg/errors v0.9.1
	gonum.org/v1/gonum v0.8.2 // indirect
)
