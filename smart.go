package markov

import (
	"errors"
	"github.com/imjasonmiller/godice"
	"sync"
)

var MaximumSmartTries = 1000

func (m *Markov) SmartSentence(doNormalization bool, routines int) (string, error) {
	if routines <= 0 {
		routines = 1
	} else if routines > 50 {
		routines = 50
	}

	var (
		err      error
		sentence string
		wg       sync.WaitGroup
	)
	wg.Add(routines)

	for i := 0; i < routines; i++ {
		go func() {
			tries := 0
			for {
				if err != nil || sentence != "" {
					break
				}

				if tries >= MaximumSmartTries {
					err = errors.New("reached maximum tries")
					break
				}

				s, err2 := m.GenerateSentences(1, doNormalization)
				if err2 != nil {
					err = err2
					break
				}
				tries++

				if !m.HasSentence(s) {
					sentence = s
					break
				}
			}

			wg.Done()
		}()
	}

	wg.Wait()

	return sentence, err
}

func (m Markov) HasSentence(sentence string) bool {
	m._mutex.Lock()
	input := m.InputData
	m._mutex.Unlock()

	for _, sen := range input {
		factor := godice.CompareString(sen, sentence)

		if factor > 0.9 {
			return true
		}
	}

	return false
}
