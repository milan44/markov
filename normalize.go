package markov

import (
	"math/rand"
	"strings"
)

func hasEnded(str []string) bool {
	if str == nil || len(str) < 1 {
		return false
	}

	switch str[len(str)-1] {
	case ".", "?", "!":
		return true
	}

	return false
}

func ucfirst(s string) string {
	return strings.ToUpper(s[:1]) + s[1:]
}

func getWeighted(mp map[string]int) string {
	if mp == nil || len(mp) == 0 {
		return ""
	}

	total := 0
	for _, val := range mp {
		total += val
	}

	if total <= 0 {
		return ""
	}

	r := rand.Intn(total)
	for item, weight := range mp {
		if r <= weight {
			return item
		}
		r -= weight
	}

	if len(mp) == 1 {
		for item := range mp {
			return item
		}
	}

	return ""
}

func getWeightedI(mp map[interface{}]int) interface{} {
	if mp == nil || len(mp) == 0 {
		return nil
	}

	total := 0
	for _, val := range mp {
		total += val
	}

	if total <= 0 {
		return nil
	}

	r := rand.Intn(total)
	for item, weight := range mp {
		if r <= weight {
			return item
		}
		r -= weight
	}

	if len(mp) == 1 {
		for item := range mp {
			return item
		}
	}

	return nil
}
