package markov

import (
	"github.com/jdkato/prose/v2"
	"github.com/mozillazg/go-unidecode"
)

func Tokenize(text string) ([]string, error) {
	doc, err := prose.NewDocument(text)
	if err != nil {
		return nil, err
	}

	tokens := make([]string, 0)

	for _, tok := range doc.Tokens() {
		tokens = append(tokens, tok.Text)
	}

	return tokens, nil
}

func TokenizeSentences(text string) ([]string, error) {
	doc, err := prose.NewDocument(text)
	if err != nil {
		return nil, err
	}

	tokens := make([]string, 0)

	for _, tok := range doc.Sentences() {
		tokens = append(tokens, tok.Text)
	}

	return tokens, nil
}

func Normalize(text string) string {
	text = unidecode.Unidecode(text)
	return text
}
