module gitlab.com/milan44/markov/image

go 1.15

require (
	gitlab.com/milan44/imgo v1.0.6
	gitlab.com/milan44/markov v1.0.5
)
