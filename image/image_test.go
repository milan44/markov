package image

import (
	"fmt"
	"gitlab.com/milan44/imgo"
	"testing"
)

func TestMarkov_Generate(t *testing.T) {
	m := New(400)

	fmt.Println("Learning input images...")
	err := m.LoadAllImageFile("./input")
	must(err)

	fmt.Println("Generating output image...")
	img := m.Generate(1000, 1000)

	fmt.Println("Saving output image...")
	err = img.SaveToFile("output.png", imgo.FileTypePNG)
	must(err)

	fmt.Println("Done")
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
