package image

import (
	"gitlab.com/milan44/imgo"
	"gitlab.com/milan44/markov"
	"os"
	"path/filepath"
	"strings"
)

type Markov struct {
	Chain markov.Markov
}

func New(lookahead int) Markov {
	return Markov{
		Chain: markov.New(lookahead),
	}
}

func (m *Markov) LoadAllImageFile(directory string) error {
	return filepath.Walk(directory, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if strings.HasSuffix(path, ".png") || strings.HasSuffix(path, ".jpg") {
			return m.LoadImageFile(path)
		}

		return nil
	})
}

func (m *Markov) LoadImageFile(image string) error {
	img, err := imgo.NewImageFromFile(image)
	if err != nil {
		return err
	}

	m.LoadImage(*img)

	return nil
}

func (m *Markov) LoadImage(image imgo.Image) {
	tokens := make([]string, 0)

	for y := 0; y < image.Height; y++ {
		for x := 0; x < image.Width; x++ {
			tokens = append(tokens, _PixelToString(image.Get(x, y)))
		}
	}

	m.Chain.LoadTokens(tokens)
}

func (m Markov) Generate(height, width int) imgo.Image {
	img := imgo.NewBlankImage(width, height)
	previous := ""

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			nextStr := m.Chain.Next(previous)
			nextSplit := strings.Split(nextStr, " ")
			next := nextSplit[len(nextSplit)-1]

			p := _StringToPixel(next)
			img.Set(x, y, p)

			previous = nextStr
		}
	}

	return img
}
