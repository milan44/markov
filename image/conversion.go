package image

import (
	"fmt"
	"gitlab.com/milan44/imgo"
	"strconv"
	"strings"
)

func _PixelToString(p imgo.Pixel) string {
	return fmt.Sprint(p.R) + "|" + fmt.Sprint(p.G) + "|" + fmt.Sprint(p.B) + "|" + fmt.Sprint(p.A)
}

func _StringToPixel(s string) imgo.Pixel {
	sp := strings.Split(s, "|")

	if len(sp) != 4 {
		return imgo.Pixel{}
	}

	r, _ := strconv.Atoi(sp[0])
	g, _ := strconv.Atoi(sp[1])
	b, _ := strconv.Atoi(sp[2])
	a, _ := strconv.Atoi(sp[3])

	return imgo.Pixel{
		R: uint8(r),
		G: uint8(g),
		B: uint8(b),
		A: uint8(a),
	}
}
