package markov

import (
	"fmt"
	"io/ioutil"
	"strings"
	"testing"
)

func TestMarkov_Sentences(t *testing.T) {
	m, err := FromFile("test.markov")

	if err != nil {
		fmt.Println("Reading text file...")
		b, err := ioutil.ReadFile("book.txt")
		must(err)

		ma := New(4)
		m = &ma

		fmt.Println("Loading text...")
		err = m.LoadText(string(b))
		must(err)
	}

	fmt.Println("Generating book...")
	cfg := BookConfig{
		CharactersPerPage: 3200,
		Pages:             1,
		MaxTitleLength:    70,
	}
	b, err := m.GenerateBook(cfg)
	must(err)

	fmt.Println("Saving book...")
	_ = ioutil.WriteFile("output.txt", []byte(b.Title+"\n------\n\n"+strings.Join(b.Pages, "\n\n")), 0777)

	fmt.Println("Saving chain...")
	err = m.ToFile("test.markov")
	must(err)
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
