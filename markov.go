package markov

import (
	"encoding/json"
	"github.com/pkg/errors"
	"io/ioutil"
	"math/rand"
	"reflect"
	"strings"
	"sync"
)

var MaximumIterations int64 = 5000

type Markov struct {
	Table          map[string]map[string]int `json:"table"`
	InputData      []string                  `json:"data"`
	Lookahead      int                       `json:"lookahead"`
	IsSentenceOver func(words []string) bool

	_mutex sync.Mutex
}

func New(lookahead int) *Markov {
	m := Markov{
		Table:          make(map[string]map[string]int),
		InputData:      make([]string, 0),
		Lookahead:      lookahead,
		IsSentenceOver: hasEnded,
	}

	return &m
}

func (m *Markov) LoadTokens(tokens []string) {
	token := ""

	for x := 0; x < len(tokens)-m.Lookahead; x++ {
		newToken := ""
		for y := 0; y < m.Lookahead; y++ {
			newToken += tokens[x+y] + " "
		}
		newToken = strings.TrimSpace(newToken)

		if token != "" {
			if m.Table[token] == nil {
				m.Table[token] = make(map[string]int)
			}

			m.Table[token][newToken]++
		}

		token = newToken
	}
}

func (m *Markov) LoadText(text string) error {
	text = Normalize(text)

	sentences, err := TokenizeSentences(text)
	if err != nil {
		return err
	}
	m.InputData = append(m.InputData, sentences...)

	tokens, err := Tokenize(text)
	if err != nil {
		return err
	}

	m.LoadTokens(tokens)

	return nil
}

func (m *Markov) getStart(table map[string]map[string]int) string {
	keys := reflect.ValueOf(table).MapKeys()
	return keys[rand.Intn(len(keys))].Interface().(string)
}

func (m *Markov) Next(previous string, table map[string]map[string]int) string {
	if previous == "" {
		return m.getStart(table)
	}

	next := getWeighted(table[previous])

	if next == "" {
		return m.getStart(table)
	}
	return next
}

func (m *Markov) GenerateSentences(count int, doNormalization bool) (string, error) {
	m._mutex.Lock()
	table := m.Table
	m._mutex.Unlock()

	if len(table) < 1 {
		return "", errors.New("empty table")
	}

	char := m.getStart(table)
	temp := make([]string, 0)
	out := make([]string, 0)

	sentences := make([]string, 0)

	iterations := int64(0)
	dotEncounter := 0

	for {
		if iterations > MaximumIterations {
			return "", errors.New("maximum iterations hit")
		}

		newChar := m.Next(char, table)

		newChars := strings.Split(newChar, " ")
		ch := newChars[len(newChars)-1]

		temp = append(temp, ch)

		if dotEncounter > 0 {
			out = append(out, ch)

			if m.IsSentenceOver == nil || m.IsSentenceOver(out) {
				sentences = append(sentences, strings.Join(out, " "))
				out = make([]string, 0)

				if dotEncounter >= count {
					break
				}
			}
		}

		if m.IsSentenceOver == nil || m.IsSentenceOver(temp) {
			dotEncounter++
		}

		char = newChar

		iterations++
	}

	return FixSentence(sentences), nil
}

func FromFile(file string) (*Markov, error) {
	b, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	var markov Markov
	err = json.Unmarshal(b, &markov)
	if err != nil {
		return nil, err
	}

	if len(markov.Table) < 1 {
		return nil, errors.New("empty table")
	}

	return &markov, nil
}

func (m *Markov) ToFile(file string) error {
	jsonObj, _ := json.Marshal(m)
	return ioutil.WriteFile(file, jsonObj, 0777)
}
